﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using ExcelDataReader;
using System.Data;

namespace RepeatApp
{
    public static class Helper
    {
        public static bool FindElement(string label, IApp app,int minutes, int seconds)
        {
            bool element = false;
            DateTime starttime = DateTime.Now;
            DateTime waittime;
            TimeSpan t = new TimeSpan(0, minutes, seconds);
            while (element == false)
            {
                AppResult[] findelement = app.Query(c => c.Marked(label));
                element = findelement.Any();
                waittime = DateTime.Now;
                if (waittime.Subtract(starttime) > t)
                {
                    Console.WriteLine("Loading Time :" + waittime.Subtract(starttime));
                    break;
                }
            }
            return element;
        }

        public static void  LogResult(string log)
        {
            Console.WriteLine(log);
        }

        public static string[] ReadData(int testnumber,string testname)
        {
            string filePath = "InputDataforTestApp1.xlsx";
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            DataSet result = excelReader.AsDataSet();
            DataTable table = result.Tables[0];
            int rowcount = table.Rows.Count;
            int columncount = table.Columns.Count;
            String[] phonenumberarray = new String[rowcount];
            while (excelReader.Read())
            {
                int i = 0;
                foreach (DataRow dataRow in table.Rows)
                {
                    if (dataRow.ItemArray[testnumber].ToString() == testname)
                    {
                        continue;
                    }
                    else
                        phonenumberarray[i++] = dataRow.ItemArray[testnumber].ToString();
                }
            }
            excelReader.Close();
            stream.Close();
            return phonenumberarray;
        }
    }
}
