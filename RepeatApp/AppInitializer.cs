﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace RepeatApp
{
    public class AppInitializer
    {
        public static string testapkpath = "C:\\Users\\gisha\\Documents\\repeatclient\\RepeatClient\\Repeat.Android\\bin\\Release\\com.dubai1001.repeattest-Signed.apk";
        public static string liveapkpath = "";

        public static IApp StartApp(Platform platform)
        {
            if (platform == Platform.Android)
            {
                Helper.LogResult("Running the test app automation on Android");
                return ConfigureApp
                    .Android
                    .EnableLocalScreenshots()
                    .ApkFile(testapkpath) // uncomment this line for TEST APP automation
                  //.ApkFile(liveapkpath) //uncomment this line for LIVE APP automation
                    .StartApp();
            }
            else
            {
                Helper.LogResult("Running the test app automation on iOS");
                return ConfigureApp
                            .iOS
                            .AppBundle("C:\\Users\\gisha\\Documents\\repeatappautomation\\RepeatClient(1).ipa")
                            .DeviceIdentifier("DEVICE_ID_OF_SIMULATOR")
                            .StartApp();
            }
            
        }
    }
}