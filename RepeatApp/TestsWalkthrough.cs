﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace RepeatApp
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class TestsWalkthrough
    {
        IApp app;
        Platform platform;
        public static Random random = new Random(), random1 = new Random(), random2 = new Random(), random3 = new Random();
        public static string rand;
        public static string rand1;
        public static string rand2;
        public static string rand3;
        public static string rand4;
        public static string rand5;
        public static string randm;
        public static string randm1;
        public static string randm2;
        public static string randm3;

        public TestsWalkthrough(Platform platform)
        {
            this.platform = platform;
        }
        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        //[Test]
        public void O_OpenApp_VisitAllWalkthroughpages()
        {
            string[] dataArray = Helper.ReadData(14, "K_UserLogin_RedeemAllTiers_CancelRequest");
            VisitAllWalkthroughpages();
        }
        
        //[Test]
        public void P_UserLogin_HowItWorks_VisitAllWalkthroughpages()
        {
            string[] dataArray = Helper.ReadData(15, "K_UserLogin_RedeemAllTiers_CancelRequest");
            UserLogin("500000002");
            HowItWorks();
            VisitAllWalkthroughpages();
        }
        //[Test]
        public void Q_CreateAccount_HowItWorks_VisitAllWalkthroughpages()
        {
            string[] dataArray = Helper.ReadData(16, "K_UserLogin_RedeemAllTiers_CancelRequest");
            CreateAccount();
            HowItWorks();
            VisitAllWalkthroughpages();
        }
        public void HowItWorks()
        {
            app.Tap("NoResourceEntry-1");
            Helper.FindElement("How It Works",app,50,0);
            app.Tap(c => c.Marked("How It Works"));
            VisitAllWalkthroughpages();

        }
        public void VisitAllWalkthroughpages()
        {
            Helper.FindElement("Next_Automation", app, 2, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Venues", app, 1, 10);
            Assert.IsTrue(app.WaitForElement(c => c.Marked(" Venues")).Any());
        }
        public void UserLogin(string phone)
        {
            Helper.FindElement("Skip", app, 0, 30);
            app.Screenshot("Walkthrough page shown");
            app.Tap("Skip");
            Helper.FindElement("Repeat_Label_automation", app, 0, 30);
            app.Screenshot("Start Page shown");
            app.Tap("Already have an account? Log in");
            var phonenumber = app.Query("Phone Number").First();//verify that the phonenumber entering page is shown.
            app.Tap(a => a.Class("FormsEditText"));
            app.EnterText(phone);
            app.DismissKeyboard();
            app.Tap("Continue");
            Helper.FindElement("Enter your password to continue", app, 0, 40);
            app.Tap(a => a.Class("FormsEditText"));
            app.EnterText("123456");
            app.DismissKeyboard();
            app.Tap("Continue");
            Helper.FindElement(" Venues", app, 0, 30);
            Assert.IsTrue(app.WaitForElement(c => c.Marked(" Venues")).Any());

        }
        public void CreateAccount()
        {
            Helper.FindElement("Next_Automation", app, 2, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Skip_Startpage", app, 0, 30);
            app.Tap("Skip_Startpage");
            Helper.FindElement("Little Kerala - Marina1", app, 0, 30);
            app.Tap("Little Kerala - Marina1");
            Helper.FindElement("Sign up / Log in to use", app, 0, 30);
            app.Tap("Sign up / Log in to use");
            Helper.FindElement("CreateAccount_Label_Automation", app, 0, 30);
            app.Tap("CreateAccount_Label_Automation");
            Helper.FindElement("EmailEntry_Automation", app, 0, 30);
            randm1 = "test" + random1.Next(0000, 9999) + "@testrepeattest.com";
            app.EnterText(c => c.Marked("EmailEntry_Automation"), randm1);
            app.DismissKeyboard();
            Helper.FindElement("Next", app, 0, 30);
            app.Tap("Next");
            Helper.FindElement("PhoneEntry_Automtion", app, 0, 30);
            randm = "50000" + random.Next(1000, 9999);
            app.EnterText(c => c.Marked("PhoneEntry_Automtion"), randm);
            app.DismissKeyboard();
            Helper.FindElement("Next", app, 0, 30);
            app.Tap("Next");
            Helper.FindElement("FirstNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("FirstNumberEntry_automation"), "0");
            Helper.FindElement("SecondNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("SecondNumberEntry_automation"), "0");
            Helper.FindElement("ThirdNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("ThirdNumberEntry_automation"), "0");
            Helper.FindElement("FourthNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("FourthNumberEntry_automation"), "0");
            Helper.FindElement("Next", app, 0, 30);
            app.Tap("Next");
            Helper.FindElement("PasswordEntry_automation", app, 0, 30);
            app.Tap("PasswordEntry_automation");
            app.EnterText(c => c.Marked("PasswordEntry_automation"), "123456");
            app.DismissKeyboard();
            Helper.FindElement("Eye_automation", app, 0, 30);
            app.Tap("Eye_automation");
            Helper.FindElement("Next", app, 0, 30);
            app.Tap("Next");
            Helper.FindElement("FirstNameEntry_Automation", app, 0, 30);
            const string original = "michaeltest";
            rand = new string(original.ToCharArray().
            OrderBy(s => (random2.Next(2) % 2) == 0).ToArray());
            app.EnterText(c => c.Marked("FirstNameEntry_Automation"), rand);
            Helper.FindElement("LastNameEntry_Automation", app, 0, 30);
            const string original1 = "johnathantest";
            rand1 = new string(original1.ToCharArray().
            OrderBy(s => (random3.Next(2) % 2) == 0).ToArray());
            app.EnterText(c => c.Marked("LastNameEntry_Automation"), rand1);
            app.DismissKeyboard();
            Helper.FindElement("Create Account", app, 0, 30);
            app.Tap("Create Account");
        }

    }
}

