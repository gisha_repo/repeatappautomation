﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace RepeatApp
{
    [TestFixture(Platform.Android)]
   // [TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;
        public static Random random = new Random();
        public static string randfnameedit;
        public static string randlnameedit;
        public static string randphnedit;
        
        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform );
        }

        [Test]
        public void AA_UserLogin_FullFlow()
        {
            string redeembuttontext;
            string[] dataArray = Helper.ReadData(0, "AA_UserLogin_FullFlow");
            M_SkipLogin_VerifyDeafultSearchText(); 
            UserPageUserLoggedOut();
            UserLogin(dataArray);
            //VerifyNewKindSearchText("Kind_Automation");
            //VerifyNewCuisineSearchText("AutomationCuisine");
           // VerifyNewDietarySearchText("Dietary_Automation");
           // VerifyNewCommonplateSearchText("CommonPlate_Automation");
           // VerifyNewRegionSearchText("AutomationRegion");
            
           // try
           // {
           //     VerifyNewBrandSearchText("AutomationBrand");
           // }
            //catch (Exception e)
           // {
           //     Helper.LogResult("The user is admin. Hence can see the inactive brand. The default search text appears only when a brand doesn't have any active venues to display");
           // }
            VerifyHistoryandMyVenues();
            //UserPageVerificationLogin();
            HowItWorks();
          
            redeembuttontext=RedeemFTDRequest();
            CancelRequest(redeembuttontext);
            redeembuttontext = RedeemTier1();
            CancelRequest(redeembuttontext);
            redeembuttontext = RedeemTier2();
            CancelRequest(redeembuttontext);
            redeembuttontext = RedeemTier3();
            CancelRequest(redeembuttontext);
            redeembuttontext = RedeemTier4();
            CancelRequest(redeembuttontext);
            redeembuttontext = RedeemTierUnlimited();
            CancelRequest(redeembuttontext);
            Logout();
        }
        //[Test]
        public void A_CreateAccount()
        {
            string[] dataArray=Helper.ReadData(1,"A_CreateAccount");
            CreateAccount(dataArray);
        }
        //[Test]
        public void B_UserLogin()
        {
            string[] dataArray = Helper.ReadData(2,"B_UserLogin");
            SkipWalkthrough();
            UserLogin(dataArray);
        }
        //[Test]
        public void C_CreateAccount_UserPageVerfication_Logout()
        {
            string[] dataArray = Helper.ReadData(3,"C_CreateAccount_UserPageVerfication_Logout");
            string[] userdetails = CreateAccount(dataArray);
            UserPageVerification(userdetails);
            Logout();
        }
        //[Test]
        public void D_UserLogin_UserPageVerificationLogin_Logout()
        {
            string[] dataArray = Helper.ReadData(4,"D_UserLogin_UserPageVerificationLogin_Logout");
            SkipWalkthrough();
            UserLogin(dataArray);
            UserPageVerificationLogin();
            Logout();
        }
        //[Test]
        public void E_CreateAccount_EditProfile()
        {
            string[] dataArray = Helper.ReadData(5,"E_CreateAccount_EditProfile");
            CreateAccount(dataArray);
            EditProfile();
        }
        //[Test]
        public void F_UserLogin_EditProfile()
        {
            string[] dataArray = Helper.ReadData(6,"F_UserLogin_EditProfile");
            SkipWalkthrough();
            UserLogin(dataArray);
            EditProfile();
        }
        //[Test]
        public void G_UserLogin_VerifyHistoryandMyVenues()
        {
            string[] dataArray = Helper.ReadData(7,"G_UserLogin_VerifyHistoryandMyVenues");
            SkipWalkthrough();
            UserLogin(dataArray);
            VerifyHistoryandMyVenues();
        }
        //[Test]
        public void H_ForgotPassword()
        {
            string[] dataArray = Helper.ReadData(8,"H_ForgotPassword");
            ForgotPassword();
        }
        //[Test]
        public void I_CreateAccount_RedeemFTDRequest_CancelRequest()
        {
            string[] dataArray = Helper.ReadData(9,"I_CreateAccount_RedeemFTDRequest_CancelRequest");
            CreateAccount(dataArray);
            string redeembuttontext=RedeemFTDRequest();
            CancelRequest(redeembuttontext);
        }

        //[Test]
        public void J_UserLogin_RedeemFTDRequest_CancelRequest()
        {
            string[] dataArray = Helper.ReadData(10,"J_UserLogin_RedeemFTDRequest_CancelRequest");
            SkipWalkthrough();
            UserLogin(dataArray);
            string redeembuttontext=RedeemFTDRequest();
            CancelRequest(redeembuttontext);
        }
        //[Test]
        public void K_UserLogin_RedeemAllTiers_CancelRequest()
        {
            string redeembuttontext;
            string[] dataArray = Helper.ReadData(11,"K_UserLogin_RedeemAllTiers_CancelRequest");
            SkipWalkthrough();
            UserLogin(dataArray);
            redeembuttontext= RedeemTier1();
            CancelRequest(redeembuttontext);
            redeembuttontext=RedeemTier2();
            CancelRequest(redeembuttontext);
            redeembuttontext=RedeemTier3();
            CancelRequest(redeembuttontext);
            redeembuttontext=RedeemTier4();
            CancelRequest(redeembuttontext);
            redeembuttontext=RedeemTierUnlimited();
            CancelRequest(redeembuttontext);
        }
        //[Test]
        public void L_FBUserSetPassword_Logout_UserLogin_VerfiyFbUserProfile()
        {
            string[] dataArray = Helper.ReadData(12, "K_UserLogin_RedeemAllTiers_CancelRequest");
            FBUserSetPassword("500000071");
            Logout();
            UserLogin(dataArray);
            VerifyFbUserProfile();
        }
        //[Test]
        public void M_SkipLogin_VerifyDeafultSearchText()
        {
            //string[] dataArray = Helper.ReadData(13, "M_SkipLogin_VerifyDeafultSearchText");
            SkipLogin();
            VerifyNewKindSearchText("Kind_Automation");
            VerifyNewCuisineSearchText("AutomationCuisine");
            VerifyNewDietarySearchText("Dietary_Automation");
            VerifyNewCommonplateSearchText("CommonPlate_Automation");
            VerifyNewRegionSearchText("Region_Automation");
            VerifyNewBrandSearchText("AutomationBrand");
        }
        //[Test]
        public void N_UserLogin_VerifyDeafultSearchText()
        {
            string[] dataArray = Helper.ReadData(14, "N_UserLogin_VerifyDeafultSearchText");
            SkipWalkthrough();
            UserLogin(dataArray);//pass a non admin user phone number
            VerifyNewKindSearchText("AutomationKind");
            VerifyNewCuisineSearchText("AutomationCuisine");
            VerifyNewDietarySearchText("AutomationDietary");
            VerifyNewCommonplateSearchText("AutomationCommonPlate");
            VerifyNewRegionSearchText("AutomationRegion");
            try
            {
                VerifyNewBrandSearchText("AutomationBrand");
            }
            catch (Exception e)
            {
                Helper.LogResult("The user is admin. Hence can see the inactive brand. The default search text appears only when a brand doesn't have any active venues to display");
            }
        }
        //[Test]
        public void O_OpenApp_VisitAllWalkthroughpages()
        {
            // string[] dataArray = Helper.ReadData(15, "O_OpenApp_VisitAllWalkthroughpages");
            VisitAllWalkthroughpages();
        }

        //[Test]
        public void P_UserLogin_HowItWorks_VisitAllWalkthroughpages()
        {
            string[] dataArray = Helper.ReadData(16, "P_UserLogin_HowItWorks_VisitAllWalkthroughpages");
            SkipWalkthrough();
            UserLogin(dataArray);
            HowItWorks();
            VisitAllWalkthroughpages();
        }
        //[Test]
        public void Q_CreateAccount_HowItWorks_VisitAllWalkthroughpages()
        {
            string[] dataArray = Helper.ReadData(17, "Q_CreateAccount_HowItWorks_VisitAllWalkthroughpages");
            CreateAccount(dataArray);
            HowItWorks();
            VisitAllWalkthroughpages();
        }


        public string[] CreateAccount(string[] phonenumberarray)
        {
            string[] userDetails = new string[4];
            app.Screenshot("App is launched");
            Helper.FindElement("Next_Automation", app, 5, 30);
            app.Tap("Next_Automation");
            app.Screenshot("Walkthrough 1");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            app.Screenshot("Walkthrough 2");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            app.Screenshot("Walkthrough 3");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            app.Screenshot("Walkthrough 4");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            app.Screenshot("Walkthrough 5");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            app.Screenshot("Walkthrough 6");
            Helper.FindElement("Skip_Startpage", app, 0, 30);
            app.Tap("Skip_Startpage");
            app.Screenshot("skip tapped. Home page shown");
            do
            {
                app.ScrollDown();
                Thread.Sleep(1000);
            } while (!app.Query("Little Kerala - Marina1").Any());
                
            app.Screenshot("Venue Found");
            app.Tap("Little Kerala - Marina1");
            //app.ScrollDownTo(c => c.Marked("Little Kerala - Marina1"), timeout: TimeSpan.FromSeconds(1200));
            //app.Screenshot("Venue Found");
            //Helper.FindElement("Little Kerala - Marina1", app, 3, 0);
            //app.Tap("Little Kerala - Marina1");
            app.Screenshot("venue tapped");
            Helper.FindElement("Sign up / Log in to use", app, 3, 0);
            app.Screenshot("Sign up / Log in to use");
            app.Tap("Sign up / Log in to use");
            app.Screenshot("signup/login tapped");
            Helper.FindElement("CreateAccount_Label_Automation", app, 3, 0);
            app.Screenshot("Start page shown");
            app.Tap("CreateAccount_Label_Automation");
            app.Screenshot("create account tapped");
            Helper.FindElement("EmailEntry_Automation", app, 3, 0);
            app.Screenshot("Email entered");
            userDetails[0] = "test" + random.Next(0000, 9999) + "@testrepeattest.com";
            app.EnterText(c => c.Marked("EmailEntry_Automation"), userDetails[0]);
            app.DismissKeyboard();
            Helper.FindElement("Next", app, 3, 0);
            app.Tap("Next");
            app.Screenshot("email next tapped");
            
            Helper.FindElement("PhoneEntry_Automtion", app, 0, 30);

            for (int i = 0; i <= phonenumberarray.Length; i++)
            {
                //randm = "50000" + random.Next(1000, 9999);
                app.EnterText(c => c.Marked("PhoneEntry_Automtion"), phonenumberarray[i]);
                app.Screenshot("phone number entered");
                app.DismissKeyboard();
                Helper.FindElement("Next", app, 0, 30);
                app.Tap("Next");
                app.Screenshot("");
                if (Helper.FindElement("Verify your number", app, 0, 10) == true)
                {
                    userDetails[1] = phonenumberarray[i];
                    break;
                }
                else
                    app.ClearText(c => c.Class("FormsEditText"));
                continue;
            }
            app.Screenshot("Phone number accepted");
            Helper.FindElement("FirstNumberEntry_automation", app, 0, 30);
            app.Screenshot("OTP is asked for ");
            app.EnterText(c => c.Marked("FirstNumberEntry_automation"), "0");
            Helper.FindElement("SecondNumberEntry_automation", app, 3, 0);
            app.EnterText(c => c.Marked("SecondNumberEntry_automation"), "0");
            Helper.FindElement("ThirdNumberEntry_automation", app, 3, 0);
            app.EnterText(c => c.Marked("ThirdNumberEntry_automation"), "0");
            Helper.FindElement("FourthNumberEntry_automation", app, 3, 0);
            app.EnterText(c => c.Marked("FourthNumberEntry_automation"), "0");
            Helper.FindElement("Next", app, 0, 30);
            app.Tap("Next");
            app.Screenshot("OTP entered");
            while(app.Query("Expired verification code").Any()==true)
            {
                app.Tap("OK");
                Helper.FindElement("Next", app, 0, 30);
                app.Tap("Next");
            }
            app.Screenshot("OTP accepted");
            Helper.FindElement("PasswordEntry_automation", app, 0, 10);
            app.Tap("PasswordEntry_automation");
            app.EnterText(c => c.Marked("PasswordEntry_automation"), "123456");
            app.Screenshot("password entered");
            app.DismissKeyboard();
            Helper.FindElement("Eye_automation", app, 3, 0);
            app.Tap("Eye_automation");
            app.Screenshot("eye icon tapped");
            Helper.FindElement("Next", app, 3, 0);
            app.Tap("Next");
            app.Screenshot("pass next tapped");
            Helper.FindElement("FirstNameEntry_Automation", app, 0, 10);
            const string original = "michaeltest";
            userDetails[2] = new string(original.ToCharArray().
            OrderBy(s => (random.Next(2) % 2) == 0).ToArray());
            app.EnterText(c => c.Marked("FirstNameEntry_Automation"), userDetails[2]);
            //userDetails[2] = rand;
            Helper.FindElement("LastNameEntry_Automation", app, 0, 10);
            const string original1 = "johnathantest";
            userDetails[3] = new string(original1.ToCharArray().
            OrderBy(s => (random.Next(2) % 2) == 0).ToArray());
            app.EnterText(c => c.Marked("LastNameEntry_Automation"), userDetails[3]);
            //userDetails[3] = rand1;
            app.DismissKeyboard();
            Helper.FindElement("Create Account", app, 0, 10);
            app.Flash("Create Account");
            app.Tap("Create Account");
            app.Screenshot("Create account tapped");
            Helper.FindElement(" Venues", app, 1, 30);
            app.Screenshot("Home Page is shown");
            return userDetails;
        }

        public void SkipWalkthrough()
        {
            Helper.FindElement("Skip", app, 2, 0);
            app.Screenshot("Walkthrough page shown");
            app.Tap("Skip");
            Helper.FindElement("Repeat_Label_automation", app, 2, 0);
        }

            public void UserLogin(string[] phonenumberarray)
        {
            Helper.FindElement("Repeat_Label_automation", app, 2, 0);
            app.Screenshot("Start Page shown");
            app.Tap("Already have an account? Log in");
            var phonenumber = app.Query("Phone Number").First();//verify that the phonenumber entering page is shown.
            app.Tap(a => a.Class("FormsEditText"));
            //var pid = phonenumber?.Id;
            //var id_number = pid.Substring(16);
            //int edittext_id = Convert.ToInt32(id_number) + 4;
            //app.Tap("NoResourceEntry-" + edittext_id);
            app.EnterText(phonenumberarray[0]);
            app.Screenshot("Phone number entered");
            app.DismissKeyboard();
            app.Tap("Continue");
            app.Screenshot("Continue Clicked");
            Helper.FindElement("Enter your password to continue", app, 2, 0);
            app.Tap(a => a.Class("FormsEditText"));
            app.EnterText("123456");
            app.Screenshot("Password entered");
            app.DismissKeyboard();
            app.Tap("Continue");
            app.Screenshot("Continue Clicked");
            Helper.FindElement(" Venues", app, 2, 0);
            Assert.IsTrue(app.WaitForElement(c => c.Marked(" Venues")).Any());
            app.Screenshot("Home Page is shown");
        }

        public void UserPageVerification(string[] userdetails)
        {
            Helper.FindElement("NoResourceEntry-3", app, 0, 30);
            app.Tap("NoResourceEntry-3");
            app.WaitForElement("My details");
            app.Tap("My details");
            Assert.AreEqual(userdetails[2], app.Query(c => c.Marked("Firstnamedecrypted_automation")).First().Text);
            Assert.AreEqual(userdetails[3], app.Query(c => c.Marked("LastNameDecrypted_Automation")).First().Text);
            Assert.AreEqual("+971" + userdetails[1], app.Query(c => c.Marked("phonenumberdecrypted_Automation")).First().Text);
            Assert.AreEqual(userdetails[0], app.Query(c => c.Marked("Emaildecrypted_automation")).First().Text);
            app.Screenshot("User_Profile");
            app.Tap("editfirstname_automation");
            Helper.FindElement("First Name", app, 0, 30);
            app.DismissKeyboard();
            app.Screenshot("Name_Screen");
            var editname_id = app.Query("First Name").First();
            var name_id = editname_id?.Id;
            var name_number = name_id.Substring(16);
            int editfirstname_id = Convert.ToInt32(name_number) + 1;
            int editlastname_id = Convert.ToInt32(name_number) + 5;
            Assert.AreEqual(userdetails[2], app.Query(c => c.Marked("NoResourceEntry-" + editfirstname_id)).First().Text);
            Assert.AreEqual(userdetails[3], app.Query(c => c.Marked("NoResourceEntry-" + editlastname_id)).First().Text);
            //Assert.AreEqual(userdetails[2], app.Query(c => c.Marked("NoResourceEntry-440")).First().Text);
            //Assert.AreEqual(userdetails[3], app.Query(c => c.Marked("NoResourceEntry-444")).First().Text);
            app.Back();
            Thread.Sleep(10000);
            app.Back();
            Thread.Sleep(10000);
            Helper.FindElement("editphone_automation", app, 0, 30);
            app.Tap("editphone_automation");
            Helper.FindElement("Phone Number", app, 0, 30);
            app.DismissKeyboard();
            app.Screenshot("Phonenum_Screen");
            var editph_id = app.Query("Phone Number").First();
            var ph_id1 = editph_id?.Id;
            var phone_id = ph_id1.Substring(16);
            int editph_id1 = Convert.ToInt32(phone_id) + 4;
            Assert.AreEqual(userdetails[1], app.Query(c => c.Marked("NoResourceEntry-" + editph_id1)).First().Text);
            //Assert.AreEqual(userdetails[1], app.Query(c => c.Marked("NoResourceEntry-457")).First().Text);
            app.Back();
            Thread.Sleep(10000);
            app.Back();
            Thread.Sleep(10000);
            Helper.FindElement("emailedit_automation", app, 2, 0);
            app.Tap("emailedit_automation");
            Helper.FindElement("Email", app, 2, 0);
            app.DismissKeyboard();
            app.Screenshot("Email_Screen");
            var email_id = app.Query("Email").First();
            var em_id = email_id?.Id;
            var mail_id = em_id.Substring(16);
            int email_id1 = Convert.ToInt32(mail_id) + 1;
            Assert.AreEqual(userdetails[0], app.Query(c => c.Marked("NoResourceEntry-" + email_id1)).First().Text);
            //Assert.AreEqual(userdetails[0], app.Query(c => c.Marked("NoResourceEntry-467")).First().Text);
            app.Back();
            Thread.Sleep(10000);
            app.Back();
            Thread.Sleep(10000);
            Helper.FindElement("editpassword_automation", app, 2, 0);
            app.Tap("editpassword_automation");
            Helper.FindElement("Current_Password_Label_Automation", app, 2, 0);
            app.DismissKeyboard();
            app.Screenshot("Pass_Screen");
            Assert.AreEqual("", app.Query(c => c.Marked("Password_Entry_Automation")).First().Text);
            app.Screenshot("User's current password is not shown by default");
            app.Back();
            app.Screenshot("clicked back");
            Thread.Sleep(10000);
            app.Back();
            app.Screenshot("clicked back");
            
            Thread.Sleep(10000);
            app.Back();
            app.Screenshot("clicked back");
            Thread.Sleep(10000);
        }

        public void VerifyHistoryandMyVenues()
        {
            app.Tap("NoResourceEntry-2");
            if (app.Query("No Venues").Any())
            {
                Assert.IsFalse(app.Query("History").Any());
                Assert.IsTrue(app.Query("Discover more").Any());
                Helper.LogResult("New User with no history of visits.");
                Helper.LogResult("Discover More was correctly shown .");
            }
            else
            {
                app.Query("History");
                app.Tap("History");
                app.Query("Total Saved");
                app.Query("AED");
                app.Query("Redemptions");
                app.Query("Average");
                if (!app.Query(c => c.Marked("Discover more")).Any())
                    Helper.LogResult("Discover More is not shown for user who has visited more than 2 venues."); // depends on the user login
                else
                    Helper.LogResult("Discover More is shown for user who has visited upto 2 venues.");//depends on the user logged in
                Helper.LogResult("History Page is shown for user who has visited atleast one venue.");

            }

        }


        public void Logout()
        {
            Helper.FindElement("NoResourceEntry-3", app, 0, 10);
            app.Tap("NoResourceEntry-3");
            app.WaitForElement("My details");
            Helper.FindElement("Log out", app, 0, 5);
            app.Tap("Log out");
            Helper.FindElement("Yes, sign me out", app, 0, 5);
            app.Tap("Yes, sign me out");
            app.Screenshot("Logged Out");
        }

        public string RedeemFTDRequest()//parameterize for the selected venue
        {
            string brandname, venuename;
            string redeembuttontext;
            bool requestsent = false;
            app.Tap("NoResourceEntry-0");
            Assert.IsTrue(app.WaitForElement(c => c.Marked("Venues_Title_automation")).Any());
            app.Query("VenueImage_automation");
            do
            {

                while (!app.Query("Lego - Dubai").Any())
                {
                    app.ScrollDown();
                    Thread.Sleep(1000);

                }
                app.Tap("Lego - Dubai");
                var redeem_button_text = app.Query(a => a.Class("FormsTextView")).Last().Text;
                redeembuttontext = redeem_button_text.ToString();
                if (redeem_button_text == "USE FIRST TIME DISCOUNT")
                {
                    var title = app.Query(a => a.Class("FormsTextView")).First().Text;
                    var Cardtitle = title.Split('-');
                    int i = 0;
                    string[] names = { null, null, null };
                    foreach (var item in Cardtitle)
                    {
                        names[i] = item.ToString();
                        i++;
                    }
                    brandname = names[0];
                    venuename = names[1];
                    Helper.LogResult("User successfully sent First Time Discount request from " + brandname.Trim() + venuename.Trim());
                    app.Tap("USE FIRST TIME DISCOUNT");
                    Helper.FindElement("Are you sure you want to redeem your first time discount?", app, 0, 5);
                    app.Tap("USE");
                    app.Query("Requesting");
                    requestsent = true;
                }
                else
                    app.Back();
            } while (requestsent == false);
            return redeembuttontext;
        }

        public void CancelRequest(string redeembuttontext)
        {
            app.Query("Requesting");
            app.Screenshot("");
            Helper.FindElement("Cancel Request", app, 0, 5);
            app.Tap("Cancel Request");
            app.Screenshot("");
            app.Query("Are you sure?");
            app.Query("Yes, cancel request");
            app.Tap("Yes, cancel request");
            app.Screenshot("");
            Assert.IsTrue(app.Query(redeembuttontext).Any());
            app.Screenshot("");
            string VenueCloseButtonId= app.Query(c => c.Class("CachedImageFastRenderer")).Last().Id;
            app.Tap(VenueCloseButtonId);
            app.Tap("NoResourceEntry-0");
            app.Query(" Venues");           
        }


        public void UserPageVerificationLogin()
        {
            app.Screenshot("");
            Helper.FindElement("NoResourceEntry-3", app, 0, 50);
            app.Tap("NoResourceEntry-3");
            app.Screenshot("");
            var my_details = app.Query("My details").First();
            var my_id = my_details?.Id;
            var det_id = my_id.Substring(16);
            int dis_nameid = Convert.ToInt32(det_id) - 2;
            Assert.AreEqual("Muhammad", app.Query(c => c.Marked("NoResourceEntry-" + dis_nameid)).First().Text);
            app.Screenshot("");
            app.WaitForElement("My details");
            app.Tap("My details");
            app.Screenshot("");
            Assert.AreEqual("Muhammad", app.Query(c => c.Marked("Firstnamedecrypted_automation")).First().Text);
            app.Screenshot("");
            Assert.AreEqual("Daud", app.Query(c => c.Marked("LastNameDecrypted_Automation")).First().Text);
            app.Screenshot("");
            Assert.AreEqual("+971500000008", app.Query(c => c.Marked("phonenumberdecrypted_Automation")).First().Text);
            app.Screenshot("");
            Assert.AreEqual("muhammad@1001dubai.com", app.Query(c => c.Marked("Emaildecrypted_automation")).First().Text);
            app.Screenshot("LoggedUserDetails");
            app.Tap("editfirstname_automation");
            app.DismissKeyboard();
            app.Screenshot("LoggedinName");
            var names1_id = app.Query("First Name").First();
            var names2_id = names1_id?.Id;
            var names_id = names2_id.Substring(16);
            int firs_name_id = Convert.ToInt32(names_id) + 1;
            int las_name_id = Convert.ToInt32(names_id) + 5;
            Assert.AreEqual("Muhammad", app.Query(c => c.Marked("NoResourceEntry-" + firs_name_id)).First().Text);
            app.Screenshot("");
            Assert.AreEqual("Daud", app.Query(c => c.Marked("NoResourceEntry-" + las_name_id)).First().Text);
            app.Screenshot("");
            app.Back();
            app.Screenshot("");
            Thread.Sleep(10000);
            app.Back();
            app.Screenshot("");
            Thread.Sleep(10000);
            app.WaitForElement("Your details");
            app.Tap("editphone_automation");
            app.DismissKeyboard();
            app.Screenshot("LoggedinNumber");
            var ph1_id = app.Query("Phone Number").First();
            var ph2_id = ph1_id?.Id;
            var ph3_id = ph2_id.Substring(16);
            int code_id = Convert.ToInt32(ph3_id) + 2;
            int num1_id = Convert.ToInt32(ph3_id) + 4;
            int ver_code = Convert.ToInt32(ph3_id) + 7;
            Assert.AreEqual("+971", app.Query(c => c.Marked("NoResourceEntry-" + code_id)).First().Text);
            app.Screenshot("");
            Assert.AreEqual("500000008", app.Query(c => c.Marked("NoResourceEntry-" + num1_id)).First().Text);
            app.Screenshot("");
            Assert.AreEqual("Send verification code", app.Query(c => c.Marked("NoResourceEntry-" + ver_code)).First().Text);
            app.Screenshot("");
            app.Back();
            app.Screenshot("");
            Thread.Sleep(10000);
            app.Back();
            app.Screenshot("");
            Thread.Sleep(10000);
            app.WaitForElement("Your details");
            app.Tap("emailedit_automation");
            app.DismissKeyboard();
            app.Screenshot("LoggedinEmail");
            var em_id1 = app.Query("Email").First();
            var em_id2 = em_id1?.Id;
            var em_id3 = em_id2.Substring(16);
            int email_id = Convert.ToInt32(em_id3) + 1;
            int ver_code1 = Convert.ToInt32(em_id3) + 4;
            Assert.AreEqual("muhammad@1001dubai.com", app.Query(c => c.Marked("NoResourceEntry-" + email_id)).First().Text);
            app.Screenshot("User email id verified");
            Assert.AreEqual("Send verification code", app.Query(c => c.Marked("NoResourceEntry-" + ver_code1)).First().Text);
            app.Back();
            Thread.Sleep(10000);
            app.Back();
            Thread.Sleep(10000);
            app.WaitForElement("Your details");
            app.Tap("editpassword_automation");
            app.Screenshot("Passwordedit tapped");
            app.DismissKeyboard();
            app.Screenshot("LoggedinPassword");
            Assert.AreEqual("", app.Query(c => c.Marked("Password_Entry_Automation")).First().Text);
            app.Screenshot("Current password shown empty");
            var pass_id1 = app.Query("Password_Entry_Automation").First();
            var pass_id2 = pass_id1?.Id;
            var pass_id3 = pass_id2.Substring(16);
            int ver_text = Convert.ToInt32(pass_id3) + 3;
            Assert.AreEqual("Verify", app.Query(c => c.Marked("NoResourceEntry-" + ver_text)).First().Text);
            app.Screenshot("verify password");
            app.Back();
            app.Screenshot("Back clicked");
            Thread.Sleep(10000);
            app.Back();
            app.Screenshot("Back clicked");
            Thread.Sleep(10000);
            app.Back();
            app.Screenshot("Back clicked");
            Thread.Sleep(10000);

        }

        public void EditProfile()
        {
            Helper.FindElement("NoResourceEntry-3", app, 0, 50);
            app.Screenshot("Home Page is shown");
            app.Tap("NoResourceEntry-3");
            app.WaitForElement("My details");
            app.Screenshot("User Page is shown");
            app.Tap("My details");
            app.WaitForElement("editfirstname_automation");
            app.Screenshot("User Details are shown on tapping My Details.");
            app.Tap("editfirstname_automation");
            app.Screenshot("Edit Firstname tapped");
            var name = app.Query("First Name").First();
            var pid = name?.Id;
            var id_number = pid.Substring(16);
            int firstnameedit_id = Convert.ToInt32(id_number) + 1;
            int lastnameedit_id = Convert.ToInt32(id_number) + 5;
            Helper.FindElement("NoResourceEntry-" + firstnameedit_id, app, 0, 30);
            app.ClearText("NoResourceEntry-" + firstnameedit_id);
            app.Screenshot("Clear text from first name field");
            app.Tap("NoResourceEntry-" + lastnameedit_id);
            app.ClearText("NoResourceEntry-" + lastnameedit_id);
            app.Screenshot("Cleartext from lastname field");
            const string original2 = "michaeledit";
            randfnameedit = new string(original2.ToCharArray().
            OrderBy(s => (random.Next(2) % 2) == 0).ToArray());
            app.EnterText(c => c.Marked("NoResourceEntry-" + firstnameedit_id), randfnameedit);
            app.Screenshot("new first name entered");
            const string original3 = "johnathanedit";
            randlnameedit = new string(original3.ToCharArray().
            OrderBy(s => (random.Next(2) % 2) == 0).ToArray());
            app.EnterText(c => c.Marked("NoResourceEntry-" + lastnameedit_id), randlnameedit);
            app.Screenshot("new last name entered");
            app.DismissKeyboard();
            app.Tap("Save");
            app.Screenshot("Successfully saved the updated names");
            Helper.FindElement("phonenumberdecrypted_Automation", app, 0, 30);
            app.Tap("editphone_automation");
            var phonenumber = app.Query("Phone Number").First();
            var phoneid = phonenumber?.Id;
            var ph_id = phoneid.Substring(16);
            int Phonenum_id = Convert.ToInt32(ph_id) + 4;
            Helper.FindElement("NoResourceEntry-" + Phonenum_id, app, 0, 30);
            app.ClearText("NoResourceEntry-" + Phonenum_id);
            app.Screenshot("editing the phone number");
            randphnedit = "50000" + random.Next(1000, 9999);
            app.EnterText(c => c.Marked("NoResourceEntry-" + Phonenum_id), randphnedit);
            app.Screenshot("new phone number entered");
            app.Tap(c => c.Marked("Send verification code"));
            app.Screenshot("phonenumber send for OTP verification");
            Helper.FindElement("FirstNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("FirstNumberEntry_automation"), "0");
            Helper.FindElement("SecondNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("SecondNumberEntry_automation"), "0");
            Helper.FindElement("ThirdNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("ThirdNumberEntry_automation"), "0");
            Helper.FindElement("FourthNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("FourthNumberEntry_automation"), "0");
            Helper.FindElement("Next", app, 0, 30);
            app.Screenshot("OTP is entered");
            app.Tap("Next");
            app.Screenshot("OTP is accepted");
            app.WaitForElement("Your details");
            Assert.AreEqual(randfnameedit, app.Query(c => c.Marked("Firstnamedecrypted_automation")).First().Text);
            Assert.AreEqual(randlnameedit, app.Query(c => c.Marked("LastNameDecrypted_Automation")).First().Text);
            Assert.AreEqual("+971" + randphnedit, app.Query(c => c.Marked("phonenumberdecrypted_Automation")).First().Text);
            app.Screenshot("Edited User Details");
            app.Tap("editpassword_automation");
            Assert.IsTrue(app.Query("Change password").Any());
            Assert.IsTrue(app.Query("Current Password").Any());
            app.Screenshot("Change password screen");
            Helper.FindElement("Password_Entry_Automation", app, 0, 30);
            app.EnterText(c => c.Marked("Password_Entry_Automation"), "123456");
            app.Tap("Verify");
            app.Screenshot("Password is verified");
            Helper.FindElement("Change password", app, 0, 30);
            var changepass = app.Query("Change password").First();
            var pass_id = changepass?.Id;
            var chpass_id = pass_id.Substring(16);
            int changepass_id = Convert.ToInt32(chpass_id) + 2;
            Helper.FindElement("NoResourceEntry-" + changepass_id, app, 0, 30);
            app.EnterText(c => c.Marked("NoResourceEntry-" + changepass_id), "123456");
            app.Screenshot("new password in entered");
            app.Tap("Set New Password");
            app.Screenshot("New password accepted");

        }

        public void ForgotPassword()
        {
            Helper.FindElement("Skip", app, 1, 30);
            app.Tap("Skip");
            Helper.FindElement("Repeat_Label_automation", app, 0, 30);
            app.Screenshot("Start Page shown");
            app.Tap("Already have an account? Log in");
            Helper.FindElement("Phone Number", app, 0, 30);
            var phonenumber = app.Query("Phone Number").First();
            var phoneid = phonenumber?.Id;
            var ph_id = phoneid.Substring(16);
            int Phonenum_id = Convert.ToInt32(ph_id) + 4;
            Helper.FindElement("NoResourceEntry-" + Phonenum_id, app, 0, 30);
            app.EnterText(c => c.Marked("NoResourceEntry-" + Phonenum_id), "500000058");
            app.DismissKeyboard();
            app.Tap("Continue");
            Helper.FindElement("Enter your password to continue", app, 0, 30);
            app.Screenshot("Password screen");
            app.Tap("Forgot?");
            Helper.FindElement("Email", app, 0, 30);
            app.Screenshot("Forgot Password Screen");
            app.DismissKeyboard();
            app.Tap("Submit");
            Helper.FindElement("Enter a valid email to continue", app, 0, 30);
            app.Screenshot("PasswordValidation");
            Assert.AreEqual("Enter a valid email to continue", app.Query(c => c.Marked("ValidEmailText_Automation")).First().Text);
            app.Tap("EmailEntry_automation");
            app.EnterText(c => c.Marked("EmailEntry_automation"), "dfsdfdsfsdf@sadasdas.com");
            app.Tap("Submit");
            Helper.FindElement("This email is not registered.", app, 0, 30);
            app.Screenshot("Not Registered Popup");
            app.Tap("OK");
            app.ClearText(c => c.Marked("EmailEntry_automation"));
            app.EnterText(c => c.Marked("EmailEntry_automation"), "hdhdhdjf@bcnfjfjf.fhdufud");
            app.DismissKeyboard();
            app.Tap("Submit");
            Helper.FindElement("Go to Log in", app, 0, 30);
            app.WaitForElement(c => c.Marked("Go to Log in"), timeout: TimeSpan.FromSeconds(1200));
            app.Screenshot("Passwod reset success");
        }

        public string RedeemTier1()
        {
            string redeembuttontext;
            Helper.FindElement(" Venues", app, 0, 50);
            Thread.Sleep(4000);
            do
            {
                app.ScrollDown();
                Thread.Sleep(1000);
            } while (!app.Query("Lee's Wok - Shatha Tower").Any());
            //app.ScrollDownTo(c => c.Marked("Lee's Wok - Shatha Tower"), timeout: TimeSpan.FromMinutes(2));
            app.WaitForElement(c => c.Marked("Lee's Wok - Shatha Tower"), timeout: TimeSpan.FromMinutes(2));
            Helper.FindElement("Lee's Wok - Shatha Tower", app, 5, 0);
            app.Screenshot("Tier 1 Venue Found");
            Thread.Sleep(3000);
            app.Tap("Lee's Wok - Shatha Tower");
            app.WaitForElement(c=>c.Marked("Open  12:00 AM-12:00 AM"), timeout: TimeSpan.FromSeconds(1200));
            var venuename = app.Query("Lee's Wok - Shatha Tower").First();
            var venueid = venuename?.Id;
            var venue_id = venueid.Substring(16);
            int tag_id = Convert.ToInt32(venue_id) + 1;
            int venuetag = Convert.ToInt32(venue_id) + 3;
            int venue_time = Convert.ToInt32(venue_id) + 8;
            int min_order = Convert.ToInt32(venue_id) + 9;
            int your_discount = Convert.ToInt32(venue_id) + 14;
            int time_rem = Convert.ToInt32(venue_id) + 15;
            int use_by = Convert.ToInt32(venue_id) + 16;
            int cur_percentage = Convert.ToInt32(venue_id) + 17;
            int use_by_date = Convert.ToInt32(venue_id) + 20;
            int tier2_dis = Convert.ToInt32(venue_id) + 22;
            int tier2_dur = Convert.ToInt32(venue_id) + 23;
            int tier2_start = Convert.ToInt32(venue_id) + 24;
            int tier2_per = Convert.ToInt32(venue_id) + 25;
            int tier2_use_by = Convert.ToInt32(venue_id) + 28;
            int tier3_dis = Convert.ToInt32(venue_id) + 30;
            int tier3_dur = Convert.ToInt32(venue_id) + 31;
            int tier3_start = Convert.ToInt32(venue_id) + 32;
            int tier3_per = Convert.ToInt32(venue_id) + 33;
            int tier3_text = Convert.ToInt32(venue_id) + 34;
            int tier3_use_by = Convert.ToInt32(venue_id) + 36;
            int redeem_text = Convert.ToInt32(venue_id) + 66;
            Assert.AreEqual("SouthAmerican • Prawn Cocktail • Grilled Chicken", app.Query(c => c.Marked("NoResourceEntry-" + tag_id)).First().Text);
            Assert.AreEqual("Shatha Tower", app.Query(c => c.Marked("NoResourceEntry-" + venuetag)).First().Text);
            Assert.AreEqual("Open  12:00 AM-12:00 AM", app.Query(c => c.Marked("NoResourceEntry-" + venue_time)).First().Text);
            Assert.AreEqual("Min order    AED 50.00", app.Query(c => c.Marked("NoResourceEntry-" + min_order)).First().Text);
            Assert.AreEqual("Your Discount", app.Query(c => c.Marked("NoResourceEntry-" + your_discount)).First().Text);
            Assert.AreEqual("Time Remaining", app.Query(c => c.Marked("NoResourceEntry-" + time_rem)).First().Text);
            Assert.AreEqual("Use by", app.Query(c => c.Marked("NoResourceEntry-" + use_by)).First().Text);
            Assert.AreEqual("65%", app.Query(c => c.Marked("NoResourceEntry-" + cur_percentage)).First().Text);
            app.Tap("NoResourceEntry-" + use_by_date);
            Assert.AreEqual("Tue, Aug 6th", app.Query(c => c.Marked("NoResourceEntry-" + use_by_date)).First().Text);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + tier2_dis)).First().Text);
            Assert.AreEqual("Duration", app.Query(c => c.Marked("NoResourceEntry-" + tier2_dur)).First().Text);
            Assert.AreEqual("Starts", app.Query(c => c.Marked("NoResourceEntry-" + tier2_start)).First().Text);
            Assert.AreEqual("40%", app.Query(c => c.Marked("NoResourceEntry-" + tier2_per)).First().Text);
            app.Tap("NoResourceEntry-" + tier2_use_by);
            Assert.AreEqual("Wed, Aug 7th", app.Query(c => c.Marked("NoResourceEntry-" + tier2_use_by)).First().Text);
            app.ScrollDown();
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + tier3_dis)).First().Text);
            Assert.AreEqual("Duration", app.Query(c => c.Marked("NoResourceEntry-" + tier3_dur)).First().Text);
            Assert.AreEqual("Starts", app.Query(c => c.Marked("NoResourceEntry-" + tier3_start)).First().Text);
            Assert.AreEqual("35%", app.Query(c => c.Marked("NoResourceEntry-" + tier3_per)).First().Text);
            Assert.AreEqual("Unlimited", app.Query(c => c.Marked("NoResourceEntry-" + tier3_text)).First().Text);
            app.Tap("NoResourceEntry-" + tier3_use_by);
            Assert.AreEqual("Sat, Aug 17th", app.Query(c => c.Marked("NoResourceEntry-" + tier3_use_by)).First().Text);
            Assert.AreEqual("USE 65% DISCOUNT", app.Query(c => c.Marked("NoResourceEntry-" + redeem_text)).First().Text);
            var redeem_button_text = app.Query(c => c.Marked("NoResourceEntry-" + redeem_text)).First().Text;
            redeembuttontext = redeem_button_text.ToString();
            app.Screenshot("Venue page");
            app.Tap("NoResourceEntry-" + redeem_text);
            Helper.FindElement("Requesting", app, 0, 50);
            var request_text = app.Query("Requesting").First();
            var req_id = request_text?.Id;
            var re_id = req_id.Substring(16);
            int reqs_id = Convert.ToInt32(re_id) - 1;
            int req_per = Convert.ToInt32(re_id) + 2;
            int dis_text = Convert.ToInt32(re_id) + 3;
            int text = Convert.ToInt32(re_id) + 4;
            Assert.AreEqual("Fortyfour user", app.Query(c => c.Marked("NoResourceEntry-" + reqs_id)).First().Text);
            Assert.AreEqual("65%", app.Query(c => c.Marked("NoResourceEntry-" + req_per)).First().Text);
            Assert.AreEqual("DISCOUNT", app.Query(c => c.Marked("NoResourceEntry-" + dis_text)).First().Text);
            Assert.AreEqual("Please ask a staff member to approve your request.", app.Query(c => c.Marked("NoResourceEntry-" + text)).First().Text);
            app.Screenshot("Requesting Screen");
            return redeembuttontext;
        }
        public string RedeemTier2()
        {
            string redeembuttontext;
            Thread.Sleep(7200);
            app.Back();
            Thread.Sleep(3000);
            do
            {
                app.ScrollUp();
                Thread.Sleep(1000);
            } while (!app.Query(" Venues").Any());
            //app.ScrollUpTo(c => c.Marked(" Venues"), timeout: TimeSpan.FromSeconds(1200));
            Helper.FindElement(" Venues", app, 5, 0);
            Thread.Sleep(4000);
            do
            {
                app.ScrollDown();
                Thread.Sleep(2000);
            } while (!app.Query("Rasam - Noor bank").Any());
            //app.ScrollDownTo(c => c.Marked("Rasam - Noor bank"), timeout: TimeSpan.FromSeconds(1200));
            Helper.FindElement("Rasam - Noor bank", app, 5, 0);
            app.Screenshot("Tie 2 Venue Found");
            app.Tap("Rasam - Noor bank");
            Helper.FindElement("Open  12:00 AM-12:00 AM", app, 5, 0);
            var venue_text1 = app.Query("Rasam - Noor bank").First();
            var venue_tags1 = venue_text1?.Id;
            var ventag_id1 = venue_tags1.Substring(16);
            int venue_tagid1 = Convert.ToInt32(ventag_id1) + 1;
            int venue_loc1 = Convert.ToInt32(ventag_id1) + 3;
            int venue_open1 = Convert.ToInt32(ventag_id1) + 8;
            int venue_minorder1 = Convert.ToInt32(ventag_id1) + 9;
            int ven_prevdistext1 = Convert.ToInt32(ventag_id1) + 14;
            int ven_exp1 = Convert.ToInt32(ventag_id1) + 15;
            int ven_prevdisc1 = Convert.ToInt32(ventag_id1) + 17;
            int ven_prev_expday_1 = Convert.ToInt32(ventag_id1) + 18;
            int ven_cur_dis = Convert.ToInt32(ventag_id1) + 22;
            int ven_cur_disc_timerem = Convert.ToInt32(ventag_id1) + 23;
            int cur_dis_useby_text = Convert.ToInt32(ventag_id1) + 24;
            int cur_des_per1 = Convert.ToInt32(ventag_id1) + 25;
            int cur_des_exp = Convert.ToInt32(ventag_id1) + 28;
            int new_des_text1 = Convert.ToInt32(ventag_id1) + 30;
            int new_des_dur_text = Convert.ToInt32(ventag_id1) + 31;
            int new_des_sta_text = Convert.ToInt32(ventag_id1) + 32;
            int new_des_per = Convert.ToInt32(ventag_id1) + 33;
            int new_des_days = Convert.ToInt32(ventag_id1) + 34;
            int new_des_expday = Convert.ToInt32(ventag_id1) + 36;
            int new1_des_text1 = Convert.ToInt32(ventag_id1) + 38;
            int new1_des_dur_text = Convert.ToInt32(ventag_id1) + 39;
            int new1_des_sta_text = Convert.ToInt32(ventag_id1) + 40;
            int new1_des_per = Convert.ToInt32(ventag_id1) + 41;
            int new1_des_days = Convert.ToInt32(ventag_id1) + 42;
            int new1_des_expday = Convert.ToInt32(ventag_id1) + 43;
            int new2_des_text1 = Convert.ToInt32(ventag_id1) + 46;
            int new2_des_dur_text = Convert.ToInt32(ventag_id1) + 47;
            int new2_des_sta_text = Convert.ToInt32(ventag_id1) + 48;
            int new2_des_per = Convert.ToInt32(ventag_id1) + 49;
            int new2_des_ul = Convert.ToInt32(ventag_id1) + 50;
            int new2_des_uldays = Convert.ToInt32(ventag_id1) + 52;
            int use_btn = Convert.ToInt32(ventag_id1) + 82;
            Assert.AreEqual("Asian Cuisine • Bagel and Lox • Biriyani", app.Query(c => c.Marked("NoResourceEntry-" + venue_tagid1)).First().Text);
            Assert.AreEqual("Noor bank", app.Query(c => c.Marked("NoResourceEntry-" + venue_loc1)).First().Text);
            Assert.AreEqual("Open  12:00 AM-12:00 AM", app.Query(c => c.Marked("NoResourceEntry-" + venue_open1)).First().Text);
            Assert.AreEqual("Min order    AED 20.00", app.Query(c => c.Marked("NoResourceEntry-" + venue_minorder1)).First().Text);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdistext1)).First().Text);
            Assert.AreEqual("Expired", app.Query(c => c.Marked("NoResourceEntry-" + ven_exp1)).First().Text);
            Assert.AreEqual("51%", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdisc1)).First().Text);
            Assert.AreEqual("1 day", app.Query(c => c.Marked("NoResourceEntry-" + ven_prev_expday_1)).First().Text);
            Assert.AreEqual("Your Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_cur_dis)).First().Text);
            Assert.AreEqual("Time Remaining", app.Query(c => c.Marked("NoResourceEntry-" + ven_cur_disc_timerem)).First().Text);
            Assert.AreEqual("Use by", app.Query(c => c.Marked("NoResourceEntry-" + cur_dis_useby_text)).First().Text);
            Assert.AreEqual("33%", app.Query(c => c.Marked("NoResourceEntry-" + cur_des_per1)).First().Text);
            app.Tap("NoResourceEntry-" + cur_des_exp);
            Assert.AreEqual("Thu, Aug 8th", app.Query(c => c.Marked("NoResourceEntry-" + cur_des_exp)).First().Text);
            app.Screenshot("First use deiscount screen");
            app.ScrollDown();
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + new_des_text1)).First().Text);
            Assert.AreEqual("Duration", app.Query(c => c.Marked("NoResourceEntry-" + new_des_dur_text)).First().Text);
            Assert.AreEqual("Starts", app.Query(c => c.Marked("NoResourceEntry-" + new_des_sta_text)).First().Text);
            Assert.AreEqual("31%", app.Query(c => c.Marked("NoResourceEntry-" + new_des_per)).First().Text);
            Assert.AreEqual("5 days", app.Query(c => c.Marked("NoResourceEntry-" + new_des_days)).First().Text);
            app.Tap("NoResourceEntry-" + new_des_expday);
            Assert.AreEqual("Fri, Aug 9th", app.Query(c => c.Marked("NoResourceEntry-" + new_des_expday)).First().Text);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + new1_des_text1)).First().Text);
            Assert.AreEqual("Duration", app.Query(c => c.Marked("NoResourceEntry-" + new1_des_dur_text)).First().Text);
            Assert.AreEqual("Starts", app.Query(c => c.Marked("NoResourceEntry-" + new1_des_sta_text)).First().Text);
            Assert.AreEqual("20%", app.Query(c => c.Marked("NoResourceEntry-" + new1_des_per)).First().Text);
            Assert.AreEqual("6 days", app.Query(c => c.Marked("NoResourceEntry-" + new1_des_days)).First().Text);
            app.Tap("NoResourceEntry-" + new1_des_days);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_text1)).First().Text);
            Assert.AreEqual("Duration", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_dur_text)).First().Text);
            Assert.AreEqual("Starts", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_sta_text)).First().Text);
            Assert.AreEqual("11%", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_per)).First().Text);
            Assert.AreEqual("Unlimited", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_ul)).First().Text);
            app.Tap("NoResourceEntry-" + new2_des_ul);
            Assert.AreEqual("Tue, Aug 20th", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_uldays)).First().Text);
            Assert.AreEqual("USE 33% DISCOUNT\r\n& RESET TO 51%", app.Query(c => c.Marked("NoResourceEntry-" + use_btn)).First().Text);
            var redeem_button_text = app.Query(c => c.Marked("NoResourceEntry-" + use_btn)).First().Text;
            redeembuttontext = redeem_button_text.ToString();
            app.Tap("NoResourceEntry-" + use_btn);
            app.Screenshot("Requesting 2nd tier discount");
            return redeembuttontext;
        }
        public string RedeemTier3()
        {
            string redeembuttontext;
            Thread.Sleep(7200);
            app.Back();
            Thread.Sleep(3000);
            do
            {
                app.ScrollUp();
                Thread.Sleep(1000);
            } while (!app.Query(" Venues").Any());
            //app.ScrollUpTo(c => c.Marked("FEATURED"), timeout: TimeSpan.FromSeconds(1200));
            //Helper.FindElement("FEATURED", app, 5, 0);
            Thread.Sleep(4000);
            /*do
            {
                app.ScrollDown();
                Thread.Sleep(1000);
            } while (!app.Query("TBJ - Edappally").Any());*/
            app.ScrollDownTo(c => c.Marked("TBJ - Edappally"),timeout: TimeSpan.FromSeconds(1200));
            Helper.FindElement("TBJ - Edappally", app, 5, 0);
            app.Screenshot("Tier 3 Venue Found");
            app.Tap("TBJ - Edappally");
            Helper.FindElement("Open  12:00 AM-12:00 AM", app, 5, 0);
            var venue_text2 = app.Query("TBJ - Edappally").First();
            var venue_tags2 = venue_text2?.Id;
            var ventag_id2 = venue_tags2.Substring(16);
            int venue_tagid2 = Convert.ToInt32(ventag_id2) + 1;
            int venue_loc2 = Convert.ToInt32(ventag_id2) + 3;
            int venue_open2 = Convert.ToInt32(ventag_id2) + 8;
            int venue_minorder2 = Convert.ToInt32(ventag_id2) + 9;
            int ven_prevdistext2 = Convert.ToInt32(ventag_id2) + 14;
            int ven_exp2 = Convert.ToInt32(ventag_id2) + 15;
            int ven_prevdisc2 = Convert.ToInt32(ventag_id2) + 17;
            int ven_prev_expday_2 = Convert.ToInt32(ventag_id2) + 18;
            int ven_prevdistext3 = Convert.ToInt32(ventag_id2) + 22;
            int ven_exp3 = Convert.ToInt32(ventag_id2) + 23;
            int ven_prevdisc3 = Convert.ToInt32(ventag_id2) + 25;
            int ven_prev_expday_3 = Convert.ToInt32(ventag_id2) + 26;
            int ven_cur_dis3 = Convert.ToInt32(ventag_id2) + 30;
            int ven_cur_disc_timerem3 = Convert.ToInt32(ventag_id2) + 31;
            int cur_dis_useby_text3 = Convert.ToInt32(ventag_id2) + 32;
            int cur_des_per3 = Convert.ToInt32(ventag_id2) + 33;
            int cur_des_exp3 = Convert.ToInt32(ventag_id2) + 36;
            int new2_des_text3 = Convert.ToInt32(ventag_id2) + 38;
            int new2_des_dur_text3 = Convert.ToInt32(ventag_id2) + 39;
            int new2_des_sta_text3 = Convert.ToInt32(ventag_id2) + 40;
            int new2_des_per3 = Convert.ToInt32(ventag_id2) + 41;
            int new2_des_ul3 = Convert.ToInt32(ventag_id2) + 42;
            int new2_des_ul_date = Convert.ToInt32(ventag_id2) + 44;
            int use_btn3 = Convert.ToInt32(ventag_id2) + 74;
            Assert.AreEqual("Jamaican • Dry Chilli Chicken • Texas Barbecue", app.Query(c => c.Marked("NoResourceEntry-" + venue_tagid2)).First().Text);
            Assert.AreEqual("Edappally", app.Query(c => c.Marked("NoResourceEntry-" + venue_loc2)).First().Text);
            Assert.AreEqual("Open  12:00 AM-12:00 AM", app.Query(c => c.Marked("NoResourceEntry-" + venue_open2)).First().Text);
            Assert.AreEqual("Min order    AED 100.00", app.Query(c => c.Marked("NoResourceEntry-" + venue_minorder2)).First().Text);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdistext2)).First().Text);
            Assert.AreEqual("Expired", app.Query(c => c.Marked("NoResourceEntry-" + ven_exp2)).First().Text);
            Assert.AreEqual("71%", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdisc2)).First().Text);
            Assert.AreEqual("1 day", app.Query(c => c.Marked("NoResourceEntry-" + ven_prev_expday_2)).First().Text);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdistext3)).First().Text);
            Assert.AreEqual("Expired", app.Query(c => c.Marked("NoResourceEntry-" + ven_exp3)).First().Text);
            Assert.AreEqual("65%", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdisc3)).First().Text);
            app.ScrollDown();
            Assert.AreEqual("1 day", app.Query(c => c.Marked("NoResourceEntry-" + ven_prev_expday_3)).First().Text);
            Assert.AreEqual("Your Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_cur_dis3)).First().Text);
            Assert.AreEqual("Time Remaining", app.Query(c => c.Marked("NoResourceEntry-" + ven_cur_disc_timerem3)).First().Text);
            Assert.AreEqual("Use by", app.Query(c => c.Marked("NoResourceEntry-" + cur_dis_useby_text3)).First().Text);
            Assert.AreEqual("61%", app.Query(c => c.Marked("NoResourceEntry-" + cur_des_per3)).First().Text);
            Assert.AreEqual("Fri, Aug 9th", app.Query(c => c.Marked("NoResourceEntry-" + cur_des_exp3)).First().Text);
            app.Tap("NoResourceEntry-" + cur_des_exp3);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_text3)).First().Text);
            Assert.AreEqual("Duration", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_dur_text3)).First().Text);
            Assert.AreEqual("Starts", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_sta_text3)).First().Text);
            Assert.AreEqual("5%", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_per3)).First().Text);
            Assert.AreEqual("Unlimited", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_ul3)).First().Text);
            app.Tap("NoResourceEntry-" + new2_des_ul3);
            Assert.AreEqual("Sat, Aug 10th", app.Query(c => c.Marked("NoResourceEntry-" + new2_des_ul_date)).First().Text);
            Assert.AreEqual("USE 61% DISCOUNT\r\n& RESET TO 71%", app.Query(c => c.Marked("NoResourceEntry-" + use_btn3)).First().Text);
            var redeem_button_text = app.Query(c => c.Marked("NoResourceEntry-" + use_btn3)).First().Text;
            redeembuttontext = redeem_button_text.ToString();
            app.Tap("NoResourceEntry-" + use_btn3);
            app.Screenshot("Requesting 3rd tier discount");
            return redeembuttontext;
        }
        public string RedeemTier4()
        {
            string redeembuttontext;
            Thread.Sleep(7200);
            app.Back();
            Thread.Sleep(3000);
            do
            {
                app.ScrollUp();
                Thread.Sleep(1000);
            } while (!app.Query(" Venues").Any());
            //app.ScrollUpTo(c => c.Marked(" Venues"), timeout: TimeSpan.FromSeconds(1200));
            Helper.FindElement(" Venues", app, 5, 0);
            Thread.Sleep(4000);
            do
            {
                app.ScrollDown();
                Thread.Sleep(1000);
            } while (!app.Query("Sheru - Wayanad").Any());
            //app.ScrollDownTo(c => c.Marked("Sheru - Wayanad"), timeout: TimeSpan.FromSeconds(1200));
            Helper.FindElement("Sheru - Wayanad", app, 5, 0);
            app.Screenshot("Tier 4 Venue Found");
            app.Tap("Sheru - Wayanad");
            Helper.FindElement("Open  12:00 AM-12:00 AM", app, 5, 0);
            var venue_text3 = app.Query("Sheru - Wayanad").First();
            var venue_tags3 = venue_text3?.Id;
            var ventag_id3 = venue_tags3.Substring(16);
            int venue_tagid3 = Convert.ToInt32(ventag_id3) + 1;
            int venue_loc3 = Convert.ToInt32(ventag_id3) + 3;
            int venue_open3 = Convert.ToInt32(ventag_id3) + 8;
            int venue_minorder3 = Convert.ToInt32(ventag_id3) + 9;
            int ven_prevdistext4 = Convert.ToInt32(ventag_id3) + 14;
            int ven_exp4 = Convert.ToInt32(ventag_id3) + 15;
            int ven_prevdisc4 = Convert.ToInt32(ventag_id3) + 17;
            int ven_prev_expday_4 = Convert.ToInt32(ventag_id3) + 18;
            int ven_prevdistext5 = Convert.ToInt32(ventag_id3) + 22;
            int ven_exp5 = Convert.ToInt32(ventag_id3) + 23;
            int ven_prevdisc5 = Convert.ToInt32(ventag_id3) + 25;
            int ven_prev_expday_6 = Convert.ToInt32(ventag_id3) + 26;
            int ven_prevdistext6 = Convert.ToInt32(ventag_id3) + 30;
            int ven_exp6 = Convert.ToInt32(ventag_id3) + 31;
            int ven_prevdisc6 = Convert.ToInt32(ventag_id3) + 33;
            int ven_prev_expday_7 = Convert.ToInt32(ventag_id3) + 34;
            int cur_destext9 = Convert.ToInt32(ventag_id3) + 38;
            int cur_des_tiimerem9 = Convert.ToInt32(ventag_id3) + 39;
            int cur_des_useby_text9 = Convert.ToInt32(ventag_id3) + 40;
            int cur_des_per9 = Convert.ToInt32(ventag_id3) + 41;
            int cur_des_date = Convert.ToInt32(ventag_id3) + 44;
            int new4_des_uldis1 = Convert.ToInt32(ventag_id3) + 46;
            int new4_des_uldur2 = Convert.ToInt32(ventag_id3) + 47;
            int new4_des_ulstart3 = Convert.ToInt32(ventag_id3) + 48;
            int new4_des_ulper4 = Convert.ToInt32(ventag_id3) + 49;
            int new4_des_ultext5 = Convert.ToInt32(ventag_id3) + 50;
            int new4_des_uldate6 = Convert.ToInt32(ventag_id3) + 52;
            int use_btn4 = Convert.ToInt32(ventag_id3) + 82;
            Assert.AreEqual("Healthy • Asian Common Plate • Baked Salmon", app.Query(c => c.Marked("NoResourceEntry-" + venue_tagid3)).First().Text);
            Assert.AreEqual("Wayanad", app.Query(c => c.Marked("NoResourceEntry-" + venue_loc3)).First().Text);
            Assert.AreEqual("Open  12:00 AM-12:00 AM", app.Query(c => c.Marked("NoResourceEntry-" + venue_open3)).First().Text);
            Assert.AreEqual("Min order    AED 25.00", app.Query(c => c.Marked("NoResourceEntry-" + venue_minorder3)).First().Text);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdistext4)).First().Text);
            Assert.AreEqual("Expired", app.Query(c => c.Marked("NoResourceEntry-" + ven_exp4)).First().Text);
            Assert.AreEqual("50%", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdisc4)).First().Text);
            Assert.AreEqual("1 day", app.Query(c => c.Marked("NoResourceEntry-" + ven_prev_expday_4)).First().Text);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdistext5)).First().Text);
            Assert.AreEqual("Expired", app.Query(c => c.Marked("NoResourceEntry-" + ven_exp5)).First().Text);
            Assert.AreEqual("40%", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdisc5)).First().Text);
            Assert.AreEqual("1 day", app.Query(c => c.Marked("NoResourceEntry-" + ven_prev_expday_6)).First().Text);
            app.Screenshot("First screen tiers");
            app.ScrollDown();
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdistext6)).First().Text);
            Assert.AreEqual("Expired", app.Query(c => c.Marked("NoResourceEntry-" + ven_exp6)).First().Text);
            Assert.AreEqual("30%", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdisc6)).First().Text);
            Assert.AreEqual("1 day", app.Query(c => c.Marked("NoResourceEntry-" + ven_prev_expday_7)).First().Text);
            Assert.AreEqual("Your Discount", app.Query(c => c.Marked("NoResourceEntry-" + cur_destext9)).First().Text);
            Assert.AreEqual("Time Remaining", app.Query(c => c.Marked("NoResourceEntry-" + cur_des_tiimerem9)).First().Text);
            Assert.AreEqual("Use by", app.Query(c => c.Marked("NoResourceEntry-" + cur_des_useby_text9)).First().Text);
            Assert.AreEqual("20%", app.Query(c => c.Marked("NoResourceEntry -" + cur_des_per9)).First().Text);
            Assert.AreEqual("Sat, Aug 10th", app.Query(c => c.Marked("NoResourceEntry -" + cur_des_date)).First().Text);
            app.Tap("NoResourceEntry -" + cur_des_date);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry -" + new4_des_uldis1)).First().Text);
            Assert.AreEqual("Duration", app.Query(c => c.Marked("NoResourceEntry -" + new4_des_uldur2)).First().Text);
            Assert.AreEqual("Starts", app.Query(c => c.Marked("NoResourceEntry -" + new4_des_ulstart3)).First().Text);
            Assert.AreEqual("10%", app.Query(c => c.Marked("NoResourceEntry -" + new4_des_ulper4)).First().Text);
            Assert.AreEqual("Unlimited", app.Query(c => c.Marked("NoResourceEntry -" + new4_des_ultext5)).First().Text);
            Assert.AreEqual("Sun, Aug 11th", app.Query(c => c.Marked("NoResourceEntry -" + new4_des_uldate6)).First().Text);
            app.Tap("NoResourceEntry -" + new4_des_uldate6);
            Assert.AreEqual("USE 20% DISCOUNT\r\n& RESET TO 50%", app.Query(c => c.Marked("NoResourceEntry-" + use_btn4)).First().Text);
            var redeem_button_text = app.Query(c => c.Marked("NoResourceEntry-" + use_btn4)).First().Text;
            redeembuttontext = redeem_button_text.ToString();
            app.Tap("NoResourceEntry-" + use_btn4);
            app.Screenshot("Requesting 4th tier discount");
            return redeembuttontext;
        }

        public string  RedeemTierUnlimited()
        {
            string redeembuttontext;
            Thread.Sleep(7200);
            app.Back();
            Thread.Sleep(3000);
            do
            {
                app.ScrollUp();
                Thread.Sleep(1000);
            } while (!app.Query(" Venues").Any());
            //app.ScrollUpTo(c=>c.Marked(" Venues"), timeout: TimeSpan.FromSeconds(1200)); ;
            Helper.FindElement(" Venues", app, 5, 0);
            /*do
            {
                app.ScrollDown();
                Thread.Sleep(1000);
            } while (!app.Query("66 Grams - DCC").Any());*/
            app.ScrollDownTo(c => c.Marked("66 Grams - DCC"), timeout: TimeSpan.FromSeconds(1200));
            Helper.FindElement("66 Grams - DCC", app, 5, 0);
            app.Screenshot("Tier Unlimited Venue Found");
            app.Tap("66 Grams - DCC");
            Helper.FindElement("Open  7:00 AM-11:30 PM", app, 5, 0);
            var venue_text = app.Query("66 Grams - DCC").First();
            var venue_tags = venue_text?.Id;
            var ventag_id = venue_tags.Substring(16);
            int venue_tagid = Convert.ToInt32(ventag_id) +1;
            int venue_loc = Convert.ToInt32(ventag_id) + 3;
            int venue_open = Convert.ToInt32(ventag_id) + 8;
            int venue_minorder = Convert.ToInt32(ventag_id) + 9;
            int ven_prevdistext = Convert.ToInt32(ventag_id) + 14;
            int ven_exp = Convert.ToInt32(ventag_id) + 15;
            int ven_prevdisc = Convert.ToInt32(ventag_id) + 17;
            int ven_prev_dis_1 = Convert.ToInt32(ventag_id) + 22;
            int ven_prev_exp_1 = Convert.ToInt32(ventag_id) + 23;
            int ven_prev_disc_1 = Convert.ToInt32(ventag_id) + 25;
            int cur_dis = Convert.ToInt32(ventag_id) + 30;
            int cur_tim_rem = Convert.ToInt32(ventag_id) + 31;
            int cur_dis_per = Convert.ToInt32(ventag_id) + 33;
            int cu_dis_day = Convert.ToInt32(ventag_id) + 34;
            int cu_use = Convert.ToInt32(ventag_id) + 66;
            Assert.AreEqual("Ukrainian • Bagel and Lox • Baked Salmon", app.Query(c => c.Marked("NoResourceEntry-" + venue_tagid)).First().Text);
            Assert.AreEqual("DCC", app.Query(c => c.Marked("NoResourceEntry-" + venue_loc)).First().Text);
            Assert.AreEqual("Open  7:00 AM-11:30 PM", app.Query(c => c.Marked("NoResourceEntry-" + venue_open)).First().Text);
            Assert.AreEqual("Min order    AED 50.00", app.Query(c => c.Marked("NoResourceEntry-" + venue_minorder)).First().Text);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdistext)).First().Text);
            Assert.AreEqual("Expired", app.Query(c => c.Marked("NoResourceEntry-" + ven_exp)).First().Text);
            Assert.AreEqual("31%", app.Query(c => c.Marked("NoResourceEntry-" + ven_prevdisc)).First().Text);
            Assert.AreEqual("Discount", app.Query(c => c.Marked("NoResourceEntry-" + ven_prev_dis_1)).First().Text);
            Assert.AreEqual("Expired", app.Query(c => c.Marked("NoResourceEntry-" + ven_prev_exp_1)).First().Text);
            Assert.AreEqual("20%", app.Query(c => c.Marked("NoResourceEntry-" + ven_prev_disc_1)).First().Text);
            app.ScrollDown();
            Assert.AreEqual("Your Discount", app.Query(c => c.Marked("NoResourceEntry-" + cur_dis)).First().Text);
            Assert.AreEqual("Time Remaining", app.Query(c => c.Marked("NoResourceEntry-" + cur_tim_rem)).First().Text);
            Assert.AreEqual("10%", app.Query(c => c.Marked("NoResourceEntry-" + cur_dis_per)).First().Text);
            Assert.AreEqual("Unlimited", app.Query(c => c.Marked("NoResourceEntry-" + cu_dis_day)).First().Text);
            Assert.AreEqual("USE 10% DISCOUNT\r\n& RESET TO 31%", app.Query(c => c.Marked("NoResourceEntry-" + cu_use)).First().Text);
            var redeem_button_text = app.Query(c => c.Marked("NoResourceEntry-" + cu_use)).First().Text;
            redeembuttontext = redeem_button_text.ToString();
            app.Screenshot("Venue tier expired Page");
            app.Tap("NoResourceEntry-" + cu_use);
            return redeembuttontext;
        }
        

        public void SkipLogin()
        {
            Helper.FindElement("Skip", app, 2, 30);
            app.Screenshot("Walkthrough page shown");
            app.Tap("Skip");
            Helper.FindElement("Repeat_Label_automation", app, 0, 30);
            app.Screenshot("Start Page shown");
            app.Tap("Skip");
            Helper.FindElement(" Venues", app, 1, 0);
            app.Screenshot("Home Page");
        }
        
        public void VerifyNewKindSearchText(string kind)
        {
            app.Tap("NoResourceEntry-1");
            app.Screenshot("Search tab tapped");
            app.Tap(c => c.Class("FormsEditText"));
            app.Screenshot("Search text box tapped");
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), kind.ToLower());
            app.Screenshot("Enter text in Serch box");
            if (app.Query("SUGGESTED").Any())
            {
                // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
                app.Query(kind);
            app.Screenshot("Searched text appeared in Suggested list ");
            app.Tap(kind);
            app.Screenshot("Tapped on Searched text appeared in Suggested list ");

            if (app.Query("Apologies, we currently don't have any " + kind + "s on our platform.").Any())
            {
                app.Screenshot("There are no results for this kind");
                app.Tap("Cancel");
            }
            else
            {
                app.Screenshot("Search result Venues list");
                //app.Repl();
                string textboxid = app.Query(c => c.Class("FormsEditText")).First().Id.Substring(16);
                int crossbuttonid= Convert.ToInt32(textboxid) + 2;
                string CrossbuttonId = "NoResourceEntry-" + crossbuttonid;
                app.Tap(CrossbuttonId);
                app.Query("Cancel");
                app.Tap("Cancel");
            }
        }
            else
            {
                if (app.Query("No result for " + kind).Any())
                    app.Screenshot("No such kind is added in panel");
            }


}
        public void VerifyNewCuisineSearchText(string cuisine)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), cuisine.ToLower());
            if (app.Query("SUGGESTED").Any())
            {
                // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
                app.Query(cuisine);
                app.Tap(cuisine);
                if (app.Query("Apologies, we currently don't have any " + cuisine + " venues on our platform.").Any())
                {
                    app.Screenshot("There are no results for this cuisine");
                    app.Tap("Cancel");
                }
                else
                {
                    app.Screenshot("Search result Venues list");
                    app.Repl();
                    string textboxid = app.Query(c => c.Class("FormsEditText")).First().Id.Substring(16);
                    int crossbuttonid = Convert.ToInt32(textboxid) + 2;
                    string CrossbuttonId = "NoResourceEntry-" + crossbuttonid;
                    app.Tap(CrossbuttonId);
                    app.Query("Cancel");
                    app.Tap("Cancel");
                }
            }
            else
            {
                if (app.Query("No result for " + cuisine).Any())
                    app.Screenshot("No such cuisine is added in panel");
            }
            
        }
        public void VerifyNewDietarySearchText(string dietary)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), dietary.ToLower());
            if (app.Query("SUGGESTED").Any())
            {
                // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
                app.Query(dietary);
            app.Tap(dietary);
            
            if (app.Query("Apologies, we currently don't have any " + dietary + " restaurants on our platform.").Any())
            {
                app.Screenshot("There are no results for this dietary");
                app.Tap("Cancel");
            }
            else
            {
                app.Screenshot("Search result Venues list");
                app.Repl();
                string textboxid = app.Query(c => c.Class("FormsEditText")).First().Id.Substring(16);
                int crossbuttonid = Convert.ToInt32(textboxid) + 2;
                string CrossbuttonId = "NoResourceEntry-" + crossbuttonid;
                app.Tap(CrossbuttonId);
                app.Query("Cancel");
                app.Tap("Cancel");
            }
}
            else
            {
                if (app.Query("No result for " + dietary).Any())
                    app.Screenshot("No such dietary is added in panel");
            }

        }
        public void VerifyNewCommonplateSearchText(string commonplate)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), commonplate.ToLower());
            if (app.Query("SUGGESTED").Any())
            {
                // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
                app.Query(commonplate);
            app.Tap(commonplate);
           
            if (app.Query("Apologies, we currently don't have any venues serving " + commonplate + " .").Any())
            {
                app.Screenshot("There are no results for this commonplate");
                app.Tap("Cancel");
            }
            else
            {
                app.Screenshot("Search result Venues list");
                app.Repl();
                string textboxid = app.Query(c => c.Class("FormsEditText")).First().Id.Substring(16);
                int crossbuttonid = Convert.ToInt32(textboxid) + 2;
                string CrossbuttonId = "NoResourceEntry-" + crossbuttonid;
                app.Tap(CrossbuttonId);
                app.Query("Cancel");
                app.Tap("Cancel");
            }
}
            else
            {
                if (app.Query("No result for " + commonplate).Any())
                    app.Screenshot("No such commonplate is added in panel");
            }
        }
        public void VerifyNewRegionSearchText(string region)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), region.ToLower());
            if (app.Query("SUGGESTED").Any())
            {
                // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
                app.Query(region);
            app.Tap(region);
            
            if (app.Query("Apologies, we currently don't have any venues in " + region + " .").Any())
            {
                app.Screenshot("There are no results for this region");
                app.Tap("Cancel");
            }
            else
            {
                app.Screenshot("Search result Venues list");
                app.Repl();
                string textboxid = app.Query(c => c.Class("FormsEditText")).First().Id.Substring(16);
                int crossbuttonid = Convert.ToInt32(textboxid) + 2;
                string CrossbuttonId = "NoResourceEntry-" + crossbuttonid;
                app.Tap(CrossbuttonId);
                app.Query("Cancel");
                app.Tap("Cancel");
            }
}
            else
            {
                if (app.Query("No result for " + region).Any())
                    app.Screenshot("No such region is added in panel");
            }
        }
        public void VerifyNewBrandSearchText(string brand)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), brand.ToLower());
            if (app.Query("SUGGESTED").Any())
            {
                // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
                app.Query(brand);
            app.Tap(brand);
            
            if (app.Query("Apologies, we currently don't have any venues that matches the search criteria.").Any())
            {
                app.Screenshot("There are no results for this brand");
                app.Tap("Cancel");
            }
            else
            {
                app.Screenshot("Search result Venues list");
                app.Repl();
                string textboxid = app.Query(c => c.Class("FormsEditText")).First().Id.Substring(16);
                int crossbuttonid = Convert.ToInt32(textboxid) + 2;
                string CrossbuttonId = "NoResourceEntry-" + crossbuttonid;
                app.Tap(CrossbuttonId);
                app.Query("Cancel");
                app.Tap("Cancel");
            }
}
            else
            {
                if (app.Query("No result for " + brand).Any())
                    app.Screenshot("No such brand is added in panel");
            }
        }
        
        public void FBUserSetPassword(string phone)
        {

            Helper.FindElement("Skip", app, 0, 30);
            app.Screenshot("Walkthrough page shown");
            app.Tap("Skip");
            Helper.FindElement("Repeat_Label_automation", app, 0, 30);
            app.Screenshot("Start Page shown");
            app.Tap("Already have an account? Log in");
            var phonenumber = app.Query("Phone Number").First();//verify that the phonenumber entering page is shown.
            app.Tap(a => a.Class("FormsEditText"));
            app.EnterText(phone);
            app.DismissKeyboard();
            app.Tap("Continue");
            Assert.IsTrue(app.Query("Log in").Any());
            Assert.IsTrue(app.Query("Enter your number to continue").Any());
            Assert.IsTrue(app.Query("Phone Number").Any());
            Assert.IsTrue(app.Query("Continue with Facebook").Any());
            Assert.IsTrue(app.Query("This number is already associated with a Facebook account.").Any());
            Assert.IsTrue(app.Query("Or, set a password for this account.").Any());
            app.Screenshot("Number is associated with facebook account, but password is not set");
            app.Tap("Or, set a password for this account.");
            Helper.FindElement("Verify your number", app, 0, 30);
            app.Screenshot("OTP verification screen before setting password for fb user is verified");
            app.Query("Please enter the 4-Digit code sent to you at +971500000020");
            Helper.FindElement("FirstNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("FirstNumberEntry_automation"), "0");
            Helper.FindElement("SecondNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("SecondNumberEntry_automation"), "0");
            Helper.FindElement("ThirdNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("ThirdNumberEntry_automation"), "0");
            Helper.FindElement("FourthNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("FourthNumberEntry_automation"), "0");
            Helper.FindElement("Next", app, 0, 30);
            app.Tap("Next");
            while (Helper.FindElement("Expired verification code", app, 0, 30) == true)
            {
                app.Tap("OK");
                app.Tap("Next");
                Thread.Sleep(2000);
            }
            app.Query("Set a password");
            app.Screenshot("OTP accepted and Password is asked");
            app.Query("Secure your account with a new password.");
            app.EnterText(c => c.Class("FormsEditText"), "123456");
            app.DismissKeyboard();
            app.Tap("Set New Password");
            app.Screenshot("Password entered successfully");
            Assert.IsFalse(app.Query("Incorrect password.").Any());
            app.Query("Venues");
            app.Screenshot("User was successfully logged into the app and taken to the home page");
        }
        public void VerifyFbUserProfile()
        {
            app.Tap("NoResourceEntry-3");
            app.Screenshot("Verfiy the user page of the fb user. It has only name and phone number details");
            app.WaitForElement("My details");
            app.Tap("My details");
            var result = app.Query("Firstnamedecrypted_automation").First();
            var firstname = result?.Text;
            result = app.Query("LastNameDecrypted_Automation").First();
            var lastname = result?.Text;
            result = app.Query("phonenumberdecrypted_Automation").First();
            var mobilenumber = result?.Text;
            var email = "";
            AppResult[] emailtitle = app.Query("Email");
            if (emailtitle.Length > 0)
            {
                result = app.Query("Emaildecrypted_automation").First();
                email = result?.Text;
            }
            else email = "Facebook User";
            app.Screenshot("Facebook User profile is verified against the expected details");
            Assert.IsFalse(app.Query("Email").Any());

        }

        public void UserPageUserLoggedOut()
        {
            app.Tap("NoResourceEntry-3");
            Helper.FindElement("Log In / Sign Up", app, 50, 0);
            app.Tap(c => c.Marked("Log In / Sign Up"));
            
        }


        public void HowItWorks()
        {
            app.Tap("NoResourceEntry-3");
            Helper.FindElement("How it works", app, 50, 0);
            app.Tap(c => c.Marked("How it works"));
            Helper.FindElement("Next_Automation", app, 2, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("How it works", app, 1, 10);
            Assert.IsTrue(app.WaitForElement(c => c.Marked("How it works")).Any());

        }
        public void VisitAllWalkthroughpages()
        {
            Helper.FindElement("Next_Automation", app, 2, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement("Next_Automation", app, 0, 30);
            app.Tap("Next_Automation");
            Helper.FindElement(" Venues", app, 1, 10);
            Assert.IsTrue(app.WaitForElement(c => c.Marked(" Venues")).Any());
        }

    }
}
