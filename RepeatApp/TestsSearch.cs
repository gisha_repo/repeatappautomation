﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace RepeatApp
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class TestsSearch
    {
        IApp app;
        Platform platform;

        public TestsSearch(Platform platform)
        {
            this.platform = platform;
        }
        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        //[Test]
        public void M_SkipLogin_VerifyDeafultSearchText()
        {
            string[] dataArray = Helper.ReadData(12, "K_UserLogin_RedeemAllTiers_CancelRequest");
            SkipLogin();
            VerifyNewKindSearchText("AutomationKind");
            VerifyNewCuisineSearchText("AutomationCuisine");
            VerifyNewDietarySearchText("AutomationDietary");
            VerifyNewCommonplateSearchText("AutomationCommonPlate");
            VerifyNewRegionSearchText("AutomationRegion");
            VerifyNewBrandSearchText("AutomationBrand");
        }
        //[Test]
        public void N_UserLogin_VerifyDeafultSearchText()
        {
            string[] dataArray = Helper.ReadData(13, "K_UserLogin_RedeemAllTiers_CancelRequest");
            UserLogin("500000003");//pass a non admin user phone number
            VerifyNewKindSearchText("AutomationKind");
            VerifyNewCuisineSearchText("AutomationCuisine");
            VerifyNewDietarySearchText("AutomationDietary");
            VerifyNewCommonplateSearchText("AutomationCommonPlate");
            VerifyNewRegionSearchText("AutomationRegion");
            try
            {
                VerifyNewBrandSearchText("AutomationBrand");
            }
            catch(Exception e)
            {
                Helper.LogResult("The user is admin. Hence can see the inactive brand. The default search text appears only when a brand doesn't have any active venues to display");
            }
        }

        public void SkipLogin()
        {
            Helper.FindElement("Skip", app, 0, 30);
            app.Screenshot("Walkthrough page shown");
            app.Tap("Skip");
            Helper.FindElement("Repeat_Label_automation", app, 0, 30);
            app.Screenshot("Start Page shown");
            app.Tap("Skip");
            Helper.FindElement(" Venues", app, 1, 0);
            

        }
        public void UserLogin(string phone)
        {
            Helper.FindElement("Skip", app, 0, 30);
            app.Screenshot("Walkthrough page shown");
            app.Tap("Skip");
            Helper.FindElement("Repeat_Label_automation", app, 0, 30);
            app.Screenshot("Start Page shown");
            app.Tap("Already have an account? Log in");
            var phonenumber = app.Query("Phone Number").First();//verify that the phonenumber entering page is shown.
            app.Tap(a => a.Class("FormsEditText"));
            app.EnterText(phone);
            app.DismissKeyboard();
            app.Tap("Continue");
            Helper.FindElement("Enter your password to continue", app, 0, 40);
            app.Tap(a => a.Class("FormsEditText"));
            app.EnterText("123456");
            app.DismissKeyboard();
            app.Tap("Continue");
            Helper.FindElement(" Venues", app, 0, 30);
            Assert.IsTrue(app.WaitForElement(c => c.Marked(" Venues")).Any());

        }



        public void VerifyNewKindSearchText(string kind)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), kind.ToLower());
            app.Query("SUGGESTED");
           // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
            app.Query(kind);
            app.Tap(kind);
            app.Query("Apologies, we currently don't have any " + kind + "s on our platform.");
            app.Tap("Cancel");
            
        }
        public void VerifyNewCuisineSearchText(string cuisine)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), cuisine.ToLower());
            app.Query("SUGGESTED");
            // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
            app.Query(cuisine);
            app.Tap(cuisine);
            app.Query("Apologies, we currently don't have any " + cuisine + " venues on our platform.");
            app.Tap("Cancel");
        }
        public void VerifyNewDietarySearchText(string dietary)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), dietary.ToLower());
            app.Query("SUGGESTED");
            // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
            app.Query(dietary);
            app.Tap(dietary);
            app.Query("Apologies, we currently don't have any " + dietary + " restaurants on our platform.");
            app.Tap("Cancel");

        }
        public void VerifyNewCommonplateSearchText(string commonplate)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), commonplate.ToLower());
            app.Query("SUGGESTED");
            // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
            app.Query(commonplate);
            app.Tap(commonplate);
            app.Query("Apologies, we currently don't have any venues serving " + commonplate + " .");
            app.Tap("Cancel");
        }
        public void VerifyNewRegionSearchText(string region)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), region.ToLower());
            app.Query("SUGGESTED");
            // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
            app.Query(region);
            app.Tap(region);
            app.Query("Apologies, we currently don't have any venues in "+ region+" .");
            app.Tap("Cancel");
        }
        public void VerifyNewBrandSearchText(string brand)
        {
            app.Tap("NoResourceEntry-1"); ;
            app.Tap(c => c.Class("FormsEditText"));
            Assert.IsTrue(app.Query("Cancel").Any());
            app.EnterText(c => c.Class("FormsEditText"), brand.ToLower());
            app.Query("SUGGESTED");
            // string Kind = kind.Substring(0, 1).ToUpper() + kind.Substring(1);
            app.Query(brand);
            app.Tap(brand);
            app.Query("Apologies, we currently don't have any venues that matches the search criteria.");
            app.Tap("Cancel");
        }
        
    }
}
