﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace RepeatApp
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class TestsFacebookUser
    {
        IApp app;
        Platform platform;
        public TestsFacebookUser(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        //[Test]
        public void L_FBUserSetPassword_Logout_UserLogin_VerfiyFbUserProfile()
        {
            string[] dataArray = Helper.ReadData(11, "K_UserLogin_RedeemAllTiers_CancelRequest");
            FBUserSetPassword("500000071");
            Logout();
            UserLogin("500000071");
            VerifyFbUserProfile();
        }
        public void FBUserSetPassword(string phone)
        {

            Helper.FindElement("Skip", app, 0, 30);
            app.Screenshot("Walkthrough page shown");
            app.Tap("Skip");
            Helper.FindElement("Repeat_Label_automation", app, 0, 30);
            app.Screenshot("Start Page shown");
            app.Tap("Already have an account? Log in");
            var phonenumber = app.Query("Phone Number").First();//verify that the phonenumber entering page is shown.
            app.Tap(a => a.Class("FormsEditText"));
            app.EnterText(phone);
            app.DismissKeyboard();
            app.Tap("Continue");
            Assert.IsTrue(app.Query("Log in").Any());
            Assert.IsTrue(app.Query("Enter your number to continue").Any());
            Assert.IsTrue(app.Query("Phone Number").Any());
            Assert.IsTrue(app.Query("Continue with Facebook").Any());
            Assert.IsTrue(app.Query("This number is already associated with a Facebook account.").Any());
            Assert.IsTrue(app.Query("Or, set a password for this account.").Any());
            app.Screenshot("Number is associated with facebook account, but password is not set");
            app.Tap("Or, set a password for this account.");
            Helper.FindElement("Verify your number", app, 0, 30);
            app.Screenshot("OTP verification screen before setting password for fb user is verified");
            app.Query("Please enter the 4-Digit code sent to you at +971500000020");
            Helper.FindElement("FirstNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("FirstNumberEntry_automation"), "0");
            Helper.FindElement("SecondNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("SecondNumberEntry_automation"), "0");
            Helper.FindElement("ThirdNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("ThirdNumberEntry_automation"), "0");
            Helper.FindElement("FourthNumberEntry_automation", app, 0, 30);
            app.EnterText(c => c.Marked("FourthNumberEntry_automation"), "0");
            Helper.FindElement("Next", app, 0, 30);
            app.Tap("Next");
            while (Helper.FindElement("Expired verification code", app, 0, 30) == true)
            {
                app.Tap("OK");
                app.Tap("Next");
                Thread.Sleep(2000);
            }
            app.Query("Set a password");
            app.Screenshot("OTP accepted and Password is asked");
            app.Query("Secure your account with a new password.");
            app.EnterText(c => c.Class("FormsEditText"), "123456");
            app.DismissKeyboard();
            app.Tap("Set New Password");
            app.Screenshot("Password entered successfully");
            Assert.IsFalse(app.Query("Incorrect password.").Any());
            app.Query("Venues");
            app.Screenshot("User was successfully logged into the app and taken to the home page");
        }
        public void VerifyFbUserProfile()
        {
            app.Tap("NoResourceEntry-3");
            app.Screenshot("Verfiy the user page of the fb user. It has only name and phone number details");
            app.WaitForElement("My details");
            app.Tap("My details");
            var result = app.Query("Firstnamedecrypted_automation").First();
            var firstname = result?.Text;
            result = app.Query("LastNameDecrypted_Automation").First();
            var lastname = result?.Text;
            result = app.Query("phonenumberdecrypted_Automation").First();
            var mobilenumber = result?.Text;
            var email = "";
            AppResult[] emailtitle = app.Query("Email");
            if (emailtitle.Length > 0)
            {
                result = app.Query("Emaildecrypted_automation").First();
                email = result?.Text;
            }
            else email = "Facebook User";
            app.Screenshot("Facebook User profile is verified against the expected details");
            Assert.IsFalse(app.Query("Email").Any());

        }


        public void UserLogin(string phone)
        {
            Helper.FindElement("Skip", app, 0, 30);
            app.Screenshot("Walkthrough page shown");
            app.Tap("Skip");
            Helper.FindElement("Repeat_Label_automation", app, 0, 30);
            app.Screenshot("Start Page shown");
            app.Tap("Already have an account? Log in");
            var phonenumber = app.Query("Phone Number").First();//verify that the phonenumber entering page is shown.
            app.Tap(a => a.Class("FormsEditText"));
            app.EnterText(phone);
            app.Screenshot("Phone number entered");
            app.DismissKeyboard();
            app.Tap("Continue");
            Helper.FindElement("Enter your password to continue", app, 0, 40);
            app.Tap(a => a.Class("FormsEditText"));
            app.EnterText("123456");
            app.Screenshot("Password entered");
            app.DismissKeyboard();
            app.Tap("Continue");
            Helper.FindElement(" Venues", app, 0, 30);
            Assert.IsTrue(app.WaitForElement(c => c.Marked(" Venues")).Any());
            app.Screenshot("User successfully logged in and  home page is loaded ");
        }

        public void Logout()
        {
            Helper.FindElement("NoResourceEntry-3", app, 0, 10);
            app.Tap("NoResourceEntry-3");
            app.WaitForElement("My details");
            Helper.FindElement("Log out", app, 0, 5);
            app.Screenshot("Log Out button is found in My details");
            app.Tap("Log out");
            Helper.FindElement("Yes, sign me out", app, 0, 5);
            app.Screenshot("Log out was clicked and user is asked to confirm");
            app.Tap("Yes, sign me out");
            app.Screenshot("User confirms  and is Logged Out");
        }

        public void RedeemFTDRequest()//parameterize for the selected venue
        {
            string brandname, venuename;
            bool requestsent = false;
            Assert.IsTrue(app.WaitForElement(c => c.Marked("Venues_Title_automation")).Any());
            app.Query("VenueImage_automation");
            app.Screenshot("Venue Profile page is shown");
            do
            {
                while (!app.Query("Little Kerala - Marina1").Any())
                {
                    app.ScrollDown();
                    Thread.Sleep(1000);

                }
                app.Tap("Little Kerala - Marina1");
                var redeem_button_text = app.Query(a => a.Class("FormsTextView")).Last().Text;
                if (redeem_button_text == "REDEEM FIRST TIME DISCOUNT")
                {
                    var title = app.Query(a => a.Class("FormsTextView")).First().Text;
                    var Cardtitle = title.Split('-');
                    int i = 0;
                    string[] names = { null, null, null };
                    foreach (var item in Cardtitle)
                    {
                        names[i] = item.ToString();
                        i++;
                    }
                    brandname = names[0];
                    venuename = names[1];
                    Helper.LogResult("User successfully sent First Time Discount request from " + brandname.Trim() + venuename.Trim());
                    app.Tap("REDEEM FIRST TIME DISCOUNT");
                    Helper.FindElement("Are you sure you want to redeem your first time discount?", app, 0, 5);
                    app.Tap("REDEEM");
                    app.Query("Requesting");
                    requestsent = true;
                }
                else
                    app.Back();
            } while (requestsent == false);

        }

        public void CancelRequest()
        {
            app.Query("Requesting");
            Helper.FindElement("Cancel Request", app, 0, 5);
            app.Tap("Cancel Request");
            app.Query("Are you sure?");
            app.Query("Yes, cancel request");
            app.Tap("Yes, cancel request");
            app.Query("REDEEM FIRST TIME DISCOUNT");
        }

            }
}
